#include "../include/kernel.cuh"
#include <stdio.h>
#include <stdlib.h>

//#define N (32*32*25)
//#define M (32*32*25)
//#define F (32*32*25)

#define N 8224
#define M 8224
#define F 8224

__global__ void multiplicar_cuda(int* A,int* B,int* C,int* D){
//__global__ void multiplicar_cuda(){
	//blockDim.x // cauntos hilos por bloque
	//blockIdx.x // que bloque heres
	//threadIdx.x // que hilo heres dentro de el bloque

	int tmp=blockDim.x * blockIdx.x + threadIdx.x;
	int suma = C[tmp];
	int x=tmp%F;
	int y=tmp/F;
	int i;
	for (i=0;i<M;i++){
		suma+=A[y*M+i]*B[F*i+x];
	}
	D[tmp]=suma;
}

void generarYmultiplicar(){
	//int A[N][M],B[M][F],C[N][F],D[N][F],i,j,k;
	int i,j,k;
	int *A,*B,*C,*D;
	A=(int *)malloc(sizeof(int)*N*M);
	B=(int *)malloc(sizeof(int)*F*M);
	C=(int *)malloc(sizeof(int)*F*N);
	D=(int *)malloc(sizeof(int)*F*N);
	if (A==NULL||B==NULL||C==NULL||D==NULL){
		printf("Ez dago memoria naikorik RAMean\n");
		return;
	}
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			//A[i][j]=1;
			A[i*N+j]=1;
		}
		for(k=0;k<F;k++){
			//C[i][k]=1;
			C[i*N+k]=1;
		}
	}
	for(i=0;i<M;i++){
		for(j=0;j<F;j++){
			if (i==j){
				B[i*M+j]=1;
			}else{
				B[i*M+j]=1;
			}
		}
	}
	int* Acu;
	int* Bcu;
	int* Ccu;
	int* Dcu;
	cudaMalloc ((void**)&Acu, sizeof(int)*N*M);
	cudaMalloc ((void**)&Bcu, sizeof(int)*F*M);
	cudaMalloc ((void**)&Ccu, sizeof(int)*F*N);
	cudaMalloc ((void**)&Dcu, sizeof(int)*F*N);
	if (Acu==NULL||Bcu==NULL||Ccu==NULL||Dcu==NULL){
		printf("Ez dago memoria naikorik GPUan\nSailatu 5 ariketako programarekin\n");
		return;
	}
	cudaMemcpy(Acu, A, sizeof(int) * N * M, cudaMemcpyHostToDevice);
	cudaMemcpy(Bcu, B, sizeof(int) * F * M, cudaMemcpyHostToDevice);
	cudaMemcpy(Ccu, C, sizeof(int) * N * F, cudaMemcpyHostToDevice);
	dim3 formaGrid = dim3(N*F/32,1,1);//dim3(32768);//numero de bloques
	dim3 formaBloque =dim3(32); //dim3(32);//aris por bloque
	//multiplicacion etre N*F tiene que ser multiplo de 32
	multiplicar_cuda<<<formaGrid,formaBloque>>>(Acu,Bcu,Ccu,Dcu);
	cudaDeviceSynchronize();
	cudaMemcpy(D, Dcu,sizeof(int) * N * F, cudaMemcpyDeviceToHost);
    printf("\n");
	for (i=0;i<N;i++){
		for(j=0; j<F; j++){
			printf(" %d",D[i*N+j]);
		}
		printf("\n");
	}
	printf("\n");
}
