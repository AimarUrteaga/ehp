#include "../include/kernel.cuh"
#include <stdio.h>
__global__ void hello_world_cuda(){
    printf("gird:%dx%dx%d Bloquea:%dx%dx%d|hello world\n",threadIdx.x,threadIdx.y,threadIdx.z,blockIdx.x,blockIdx.y,blockIdx.z);//blockDim
}

void hello_world(){
    dim3 formaGrid = dim3(2,2);
    dim3 formaBloque =dim3(16,2);
    hello_world_cuda<<<formaGrid,formaBloque>>>();
    cudaDeviceSynchronize();
}
