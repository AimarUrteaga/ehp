#include "../include/kernel.cuh"
#include <stdio.h>
#include <stdlib.h>

#define N 1024

__global__ void multiplicar_cuda(int* A,int* B,int* C){
    int i=blockIdx.x*blockDim.x+threadIdx.x;
    C[i]=A[i]*B[i];//blockIdx.x=0...32768 threadIdx.x=0...32
}

void generarYmultiplicar(){
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    int A[N*N],B[N*N],C[N*N],i;
    for (i=0;i<N*N;i++){
    	A[i]=rand()%100;
    	B[i]=rand()%100;
	if (i%10000==0){
		printf("A[%d]=%d\t",i,A[i]);
		printf("B[%d]=%d\n",i,B[i]);
	}
    }
    int* Acu;
    int* Bcu;
    int* Ccu;
    cudaMalloc ((void**)&Acu, sizeof(int)*N*N);
    cudaMalloc ((void**)&Bcu, sizeof(int)*N*N);
    cudaMalloc ((void**)&Ccu, sizeof(int)*N*N);
    //printf("Mallokak egin ditu\n");
    cudaMemcpy((void*)Acu,&A,sizeof(int)*N*N,cudaMemcpyHostToDevice);
    cudaMemcpy((void*)Bcu,&B,sizeof(int)*N*N,cudaMemcpyHostToDevice);
    dim3 formaGrid = dim3(N);//dim3(32768);
    dim3 formaBloque =dim3(1024); //dim3(32);
    cudaEventRecord(start);
    multiplicar_cuda<<<formaGrid,formaBloque>>>(Acu,Bcu,Ccu);
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaMemcpy(&C,(void*)Ccu,sizeof(int)*N*N,cudaMemcpyDeviceToHost);
    for (i=0;i<N*N;i+=10000){
	 printf("C[%d]=%d\n",i,C[i]);
    }
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    printf("Denbora(ms): %f\n",milliseconds);
}//https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1gg18fa99055ee694244a270e4d5101e95b1a03d03a676ea8ec51b9b1e193617568
