#include "../include/kernel.cuh"
#include <stdio.h>
#include <stdlib.h>

#define N 32
#define M 64

__global__ void multiplicar_cuda(int* A,int* B){
//__global__ void multiplicar_cuda(){
	//blockDim.x // cauntos hilos por bloque
	//blockIdx.x // que bloque heres
	//threadIdx.x // que hilo heres dentro de el bloque
	int x=(blockDim.x * blockIdx.x + threadIdx.x)%N;
	int y=(blockDim.x * blockIdx.x + threadIdx.x)/N;
	//y*N+x
	B[x*M+y]=A[y*N+x];
}

void generarYmultiplicar(){
	int *A,*B,i,j;
	A=(int *)malloc(sizeof(int)*N*M);
	B=(int *)malloc(sizeof(int)*N*M);
	if (A==NULL||B==NULL){
		printf("Ez dago memoria naikorik RAMean\n");
		return;
	}
	int* Acu;
	int* Bcu;
	cudaMalloc ((void**)&Acu, sizeof(int)*N*M);
	cudaMalloc ((void**)&Bcu, sizeof(int)*N*M);
	if (Acu==NULL||Bcu==NULL){
		printf("Ez dago memoria naikorik GPUan\n");
		return;
	}

	for (j=0;j<M;j++){
		for (i=0;i<N;i++){
			A[j*N+i]=i;
		}
	}
	
	cudaMemcpy(Acu, A, sizeof(int)*N*M, cudaMemcpyHostToDevice);
	dim3 formaGrid = dim3((N*M)/32,1,1);//dim3(32768);//numero de bloques
	dim3 formaBloque =dim3(32); //dim3(32);//aris por bloque
	multiplicar_cuda<<<formaGrid,formaBloque>>>(Acu,Bcu);
	cudaDeviceSynchronize();
	cudaMemcpy(B, Bcu,sizeof(int) * M * N, cudaMemcpyDeviceToHost);

	for (i=0;i<M;i++){
		for(j=0; j< N; j++){
			printf("%d ",A[i*N+j]);
		}
		printf("\n");
	}
	printf("###############################################################################################################################\n");
	for (i=0;i<N;i++){
		for(j=0; j< M; j++){
			printf("%d ",B[i*M+j]);
		}
		printf("\n");
	}
	printf("\n");
}
