#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

void printDevProp(cudaDeviceProp devProp){//https://www.cs.cmu.edu/afs/cs/academic/class/15668-s11/www/cuda-doc/html/group__CUDART__DEVICE_g5aa4f47938af8276f08074d09b7d520c.html
	printf("\n------------------------------\n");
	printf("GPGPUaren izena: %s\n", devProp.name);
	printf("GPGPUan eskuragarri dagoen memoria totala bytetan: %lu\n", devProp.totalGlobalMem);
	printf("GPGPUan dagoen streaming procesor bakoitzak eskuragarri duen memoria kantitatea bytetan: %lu\n", devProp.sharedMemPerBlock);
	printf("GPGPUaren streaming procesor bakoitzaren WARP tamaina: %d\n", devProp.warpSize);
	printf("GPGPUaren streaming procesor kopurua: %d\n", devProp.multiProcessorCount);
	printf("GPGPUan 32 biteko erregistro kopurua streaming procesor bakoitzeko: %d\n", devProp.regsPerBlock);
	printf("GPGPUan kopiatu daitekeen memoria tamaina maximoa bytetan: %lu\n", devProp.memPitch);
	printf("GPGPUan streaming procesor bakoitzeko exekutatu daitesken hari kopuru maximoa: %d\n", devProp.maxThreadsPerBlock);
	//maxThreadsDim[3]
	//maxGridSize[3]
	printf("GPGPUaren maiztasuna kilohertzetan: %d\n", devProp.clockRate);
	//totalConstMem
	if (devProp.deviceOverlap){
		printf("GPGPUak RAMa kopiatu desake programa esekutatzen duen bitartean\n");
	}else{
		printf("GPGPUak RAMa ezin du kopiatu programa esekutatzen duen bitartean\n");
	}
	if (devProp.kernelExecTimeoutEnabled){
		printf("GPGPUak exekusio denbora maximo bat du\n");
	}else{
		printf("GPGPUak ez dauka exekusio denbora maximoa\n");
	}
	if (devProp.canMapHostMemory){
		printf("GPGPUak RAMean memoria ereserbatu desake (cudaHostAlloc/cudaHostGetDevicePointer erabilgarri daude)\n");
	}else{
		printf("GPGPUak RAMean memoria ezin du ereserbatu\n");
	}
	//computeMode
	if (devProp.concurrentKernels){//https://stackoverflow.com/questions/68400590/opencl-launch-concurrent-kernels
		printf("GPGPUak programa bat baino gehiago exekutatu desake aldi berean streaming procesor desberdinetan\n");
	}else{
		printf("GPGPUko streaming procesor desberdinek programa bera exekutatu desakete\n");
	}
	if (devProp.ECCEnabled){
		printf("GPGPUak Error Correcktin Code aktibatuta du\n");
	}else{
		printf("GPGPUak Error Correcktin Code ez dago aktibatuta du\n");
	}
	/*
	printf("Gailuaren \"compute capability\" handiena: %d\n", devProp.major);
	printf("Gailuaren \"compute capability\" txikiena: %d\n", devProp.minor);
	printf("Gailuaren edukiera konstantearen tamaina: %lu\n", devProp.totalConstMem);
	*/
}

int main(){
	int i, devCount;
	cudaDeviceProp devProp;
	cudaGetDeviceCount(&devCount);
	printf("Nvidia GPGPUen kopurua: %d\n", devCount);
	for(i=0; i<devCount; i++){
		printf("%d. GPGPU:\n", i);
		cudaGetDeviceProperties(&devProp, i);
		printDevProp(devProp);
	}
	return 0;
}

