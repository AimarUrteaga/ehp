#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

#define x 2
#define y 3
#define z 2

//CUDAren bitartez exekutatuko den kodea, paraleloan.
__global__ void kalkulatu_cuda(float** R, float** A, float** B, float** C, int a, int b, int c){

        int errenkada= blockIdx.y*blockDim.y+threadIdx.y;
        int zutabea= blockIdx.x*blockDim.x+threadIdx.x;

        float gehiketa=0;//biderketen gehiketak
        /*
        * Konprobatu ea posiblea den matrizeen arteko biderketa egitea, noski, gure mainerako  guk
        * sortutako matrizeak direnez, beti funtzionatuko du. Hau egin dut, sarrera datu bezala
        * edozein motako matrizeak onartuko balira.
        */
        if(zutabea<z && errenkada<x){
                //Hari bakoitzak matrizearen elementu bat kalkulatuko du.
                for(int i=0; i<b; i++){
                        gehiketa+=A[errenkada*y+i]*B[i*z+zutabea];
                }
                R[errenkada*z+zutabea]+=gehiketa;
        }

        //printf("CUDA R-ren 1.elementua=%f\n",A[0]);
}

//Matrizeen ausazko hasieraketa gauzatuko duen funtzioa.
void sor_matrizeak(int a, int b, int c, float **A, float **B, float **C){
        /*
        * OHARRA:
        * Matrizeen biderketa egiteko, haien dimentsioak bateragarriak izan behar dira,
        * hau da, lehenengo matrizearen zutabe kopurua eta bigarren matrizearen errenkada kopurua
        * berdinak izan behar dira. Aldiz, matrizeen arteko gehiketa egiteko, matrizeek dimentsio
        * berdinak eduki beharko dituzte.
        *
        * A[x*y] x B[y*z] + C[x*z]
        */
        int i:
        float n=5.0;
        //Dinamikoki memoria erreserbatu
        (*A)=(float*)malloc(sizeof(float)*x*y);
        (*B)=(float*)malloc(sizeof(float)*y*z);
        (*C)=(float*)malloc(sizeof(float)*x*z);

        //Matrizeen ausazko hasieraketa.
        for(i=0; i<x*y; i++){
                A[i]=(float)rand()/(float*)(RAND_MAX/n);
        }
        for(i=0; i<y*z; i++){
                B[i]=(float)rand()/(float)(RAND_MAX/n);
        }
        for(i=0; i<x*z; i++){
                C[i]=(float)1.0;
        }
}

/*
* Funtzio honek, ausaz sortutako A, B eta C matrizeen arteko AxB+C eragiketa
* egingo du CUDAren bitartez eta R deituriko matrize batean gordeko du emaitza.
*/
void bektoreenArtekoEragiketa(){
        int a;
        int b;
        int c;
        float* A;
        float* B;
        float* C;
        float* R;

        float* Rcu;
        float* Acu;
        float* Bcu;
        float* Ccu;
        sor_matrizeak(a,b,c,&A,&B,&C);

        printf("0.posizioan dagoena: %f\n", A[0]);
        printf("1.posizioan dagoena: %f\n", A[1]);
        printf("2.posizioan dagoena: %f\n", A[2]);
        printf("0.posizioan dagoena: %f\n", B[0]);
        printf("2.posizioan dagoena: %f\n", B[2]);
        printf("4.posizioan dagoena: %f\n", B[4]);
        printf("0.posizioan dagoena: %f\n", C[0]);
        //Matrize bakoitzarentzat memoria erreserbatu CUDA-n.
        cudaMalloc((void**)&Acu, sizeof(float)*x*y);
        cudaMalloc((void**)&Bcu, sizeof(float)*y*z);
        cudaMalloc((void**)&Ccu, sizeof(float)*x*z);
        cudaMalloc((void**)&Rcu, sizeof(float)*x*z);
        //HOST-ean dagoen informazioa CUDA-n kopiatu.
        cudaMemcpy((void*)Acu, &A, sizeof(float)*x*y, cudaMemcpyHostToDevice);
        cudaMemcpy((void*)Bcu, &B, sizeof(float)*y*z, cudaMemcpyHostToDevice);
        cudaMemcpy((void*)Ccu, &C, sizeof(float)*x*z, cudaMemcpyHostToDevice);

        //Gure emaitza matrizea (R), x*z dimentsiotakoa izango da.
        dim3 gridShape = dim3(a*c);
        dim3 blockShape = dim3(a*c);

        kalkulatu_cuda<<<gridShape,blockShape>>>(&Rcu, &Acu, &Bcu, &Ccu, a, b, c);
        cudaDeviceSynchronize();

        //CUDAtik kalkulatutakoa HOST-ean kopiatu.
        cudaMemcpy(&R, (void*)Rcu, sizeof(float)*x*z, cudaMemcpyDeviceToHost);

        printf("Rn 0.posizioan dagoena: %f\n", R[0]);
}

int main(){
        bektoreenArtekoEragiketa();
        return 0;
}