#include "../include/kernel.cuh"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//#define N (32*32*25)
//#define M (32*32*25)
//#define F (32*32*25)

#define N 8224
#define M 8224
#define F 8224


__shared__ int compartidoA[M];

__global__ void multiplicar_cuda(int* A,int* B,int* C,int* D,int nose){
	int i;
	int cuantos=nose/32;//M
	int tmp=blockDim.x * blockIdx.x + threadIdx.x;
	int y=tmp/nose;//F
	for (i=cuantos*threadIdx.x;i<cuantos*(threadIdx.x+1);i++){
		compartidoA[i]=A[y*nose+i];
	}
	
	int suma = C[tmp];
	int x=tmp%nose;
	for (i=0;i<nose;i++){
		suma+=compartidoA[i]*B[nose*i+x];//F
	}
	D[tmp]=suma;
}





void generarYmultiplicar(){
	//int A[N][M],B[M][F],C[N][F],D[N][F],i,j,k;
	int i,k,j;
	int *A,*B,*C,*D;
	A=(int *)malloc(sizeof(int)*N*M);
	B=(int *)malloc(sizeof(int)*F*M);
	C=(int *)malloc(sizeof(int)*F*N);
	D=(int *)malloc(sizeof(int)*F*N);
	int* Acu;
	int* Bcu;
	int* Ccu;
	int* Dcu;
	cudaMalloc ((void**)&Acu, sizeof(int)*N*M);
	cudaMalloc ((void**)&Bcu, sizeof(int)*F*M);
	cudaMalloc ((void**)&Ccu, sizeof(int)*F*N);
	cudaMalloc ((void**)&Dcu, sizeof(int)*F*N);
	if (Acu==NULL||Bcu==NULL||Ccu==NULL||Dcu==NULL){
		printf("Ez dago memoria naikorik GPUan\nSailatu 5 ariketako programarekin\n");
		return;
	}
	if (A==NULL||B==NULL||C==NULL||D==NULL){
		printf("Ez dago memoria naikorik RAMean\n");
		return;
	}

	
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			//A[i][j]=1;
			A[i*N+j]=1;
		}
		for(k=0;k<F;k++){
			//C[i][k]=1;
			C[i*N+k]=1;
		}
	}
	for(i=0;i<M;i++){
		for(j=0;j<F;j++){
			if (i==j){
				B[i*M+j]=1;
			}else{
				B[i*M+j]=1;
			}
		}
	}

	cudaMemcpy(Acu, A, sizeof(int) * N * M, cudaMemcpyHostToDevice);
	cudaMemcpy(Bcu, B, sizeof(int) * F * M, cudaMemcpyHostToDevice);
	cudaMemcpy(Ccu, C, sizeof(int) * N * F, cudaMemcpyHostToDevice);
	int nose;
	cudaEvent_t start, stop;
	//cudaEventCreate(&start);
	//cudaEventCreate(&stop);
	float milliseconds;
	cudaDeviceSynchronize();
	for (nose=32;nose<=M;nose+=32){
		dim3 formaGrid = dim3((nose*nose)/32,1,1);//N*F//dim3(32768);//numero de bloques
		dim3 formaBloque =dim3(32); //dim3(32);//aris por bloque
		//multiplicacion etre N*F tiene que ser multiplo de 32
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start);
		multiplicar_cuda<<<formaGrid,formaBloque>>>(Acu,Bcu,Ccu,Dcu,nose);
		cudaEventRecord(stop);
		cudaDeviceSynchronize();
		cudaEventElapsedTime(&milliseconds, start, stop);
		printf("%d,%f\n",nose,milliseconds);



		/*cudaMemcpy(D, Dcu,sizeof(int) * N * F, cudaMemcpyDeviceToHost);
		for (i=0;i<N;i++){
			for(j=0; j<F; j++){
				printf(" %d",D[i*N+j]);
			}
			printf("\n");
		}
		printf("\n");*/
	}
}