#!/bin/bash
if (($# < 1)); then
echo "Lehenengo exekutablearen izena gero ari kopurua"
exit
fi
for i in 2 4 8 16 32
do
echo "">"$i.txt"
export OMP_NUM_THREADS=$i
./$1>>"$i.txt"
done
