/*
   EHP - IIG - Konputagailuen Ingeniaritza

   beg.c  --  serieko bertsioa

   4 begizta analizatzeko eta paralelizatzeko
******************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Konstanteak eta aldagaiak

#define N1 1200000      // 1. begizta - A, B, C, E bektoreak

#define N2 40000        // 2. begizta - D mat
#define N3 4000

#define N4 10000        // 3. begizta - irudia
#define N5 8000
#define PIXMAX 10

#define N6 6000000      // 4. begizta - H, J, L bektoreak

double  A[N1], B[N1], C[N1], E[N1];
double  D[N2][N3];
int     histo[PIXMAX], irudia[N4][N5];
double  H[N6], J[N6], L[N6];



// ERRUTINA LAGUNTZAILEAK

// Pantailaratzen du "Testua" string-a eta exkuzio-denbora (ms): t1-t0
void ExekDenbora (char *Testua, struct timespec *t0, struct timespec *t1)
{
  double  tex;

  tex = (t1->tv_sec - t0->tv_sec) + (t1->tv_nsec - t0->tv_nsec) / (double)1e9;
  printf ("%s = %1.3f ms\n\n", Testua, tex*1000);
}


// Bi prozedura aldagai globalak pantailaratzeko
void EmanBektorea (double *V, int L1, int L2)
{
  int  i;

  for (i=L1; i<(L1+L2); i++)  printf ("%8.2f", V[i]);
  printf ("\n");
}

void EmanDmatrizea ()
{
  int  i, j;

  for (i=0; i<5; i++) {
    for (j=0; j<5; j++)  printf ("%8.2f", D[i][j]);
    printf ("\n");
  }
  printf ("\n");
}


// PROGRAMA NAGUSIA

void main ()
{
  int     i, j;
  double  x, batura;
  struct timespec  t0, t1 ,t2, t3, t4;


  // Hasierako balioak

  for (i=0; i<N1; i++) 
  {
    A[i] = (double) (i % 100);
    B[i] = (double) ((N1/2-i+25) % 31);
    C[i] = 2*i % 5 - 1.0;
    E[i] = 0.0;
  }

  for (i=0; i<N2; i++)
  for (j=0; j<N3; j++)  
    D[i][j] = (double) ((i*N3+j) % 19);

  for (i=0; i<N4; i++)
  for (j=0; j<N5; j++) 
  {
    if (i%3) irudia[i][j] = (i+j) % PIXMAX;
    else     irudia[i][j] = (i+i*j) % PIXMAX;
  }

  for (i=0; i<N6; i++) 
  {
    H[i] = i%5 + 1.0;
    J[i] = i%7 + 2.2;
    L[i] = i%3 - 5.5;
  }


  clock_gettime (CLOCK_REALTIME, &t0);

// EXEKUZIOA - serieAN EGITEKO

// 1. begizta

  for (i=1; i<(N1-1); i++) 
  {
    x = B[i] / (A[i] + 50.0);
    A[i] = (x + B[i] + 1.0) / 100.0;
    C[i] = A[i+1] + C[i-1]/2;
    E[i] = 100 * x * B[i+1] / (x - 2*A[i]*B[i-1]);
  }
clock_gettime (CLOCK_REALTIME, &t2);

// 2. begizta

  batura = 0.0;
  for (i=3; i<N2; i++)
  for (j=0; j<N3; j++) 
  {
    D[i][j] = D[i-3][j] / 5.0 + x - E[j];
    if (D[i][j] < 50.0) batura += D[i][j]/500000.0;
  }
clock_gettime (CLOCK_REALTIME, &t3);

// 3. begizta

  for (i=0; i<PIXMAX; i++) histo[i] = 0;

  for (i=0; i<N4; i++)
  for (j=0; j<N5; j++) 
    histo[irudia[i][j]] ++;
clock_gettime (CLOCK_REALTIME, &t4);

// 4. begizta

  for (i=2; i<N6; i++) 
  {
    H[i] = 3.5 / (7.0/L[i-1] + 2.0/H[i]);
    L[i] = L[i] / (L[i]+2.5) + 3.5 / L[i];
    J[i] = (H[i-1]/L[i-2] + 3.5/H[i-1]) / (H[i-1] + L[i-2]);
  }

  clock_gettime (CLOCK_REALTIME, &t1);

// Emaitzak inprimatu. Bektoreen erdiko 10 osagai, histograma, eta x eta batura aldagaiak.

  printf ("\nA-->   ");
  EmanBektorea (A, N1/2, 10);
  printf ("C-->   ");
  EmanBektorea (C, N1/2, 10);
  printf ("E-->   ");
  EmanBektorea (E, N1/2, 10);

  printf ("H-->   ");
  EmanBektorea (H, N6/2, 10);
  printf ("J-->   ");
  EmanBektorea (J, N6/2, 10);
  printf ("L-->   ");
  EmanBektorea (L, N6/2, 10);

  printf ("D mat-->   \n");
  EmanDmatrizea ();

  printf ("Irudiaren histograma (*100):\n");
  for (i=0; i<PIXMAX; i++) printf ("%7d", i);
  printf ("\n");
  for (i=0; i<PIXMAX; i++) printf ("%7d", histo[i]/100);
  printf ("\n\n");

  printf ("     x = %12.5f\n", x);
  printf ("batura = %12.5f\n\n", batura);

  ExekDenbora ("\n Tosoa (serie)", &t0, &t1);
  ExekDenbora ("\n T1 (serie)", &t0, &t2);
  ExekDenbora ("\n T2 (serie)", &t2, &t3);
  ExekDenbora ("\n T3 (serie)", &t3, &t4);
  ExekDenbora ("\n T4 (serie)", &t4, &t1);
}