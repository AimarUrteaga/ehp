/*
   EHP - IIG - Konputagailuen Ingeniaritza

   konbi.c	 -- serieko bertsioa

   zenbaki konbinatorioak
   funtzio errekurtsiboak task-en bidez

   bi zenbakiak parametro gisa eman behar dira exekutatzerakoan

   PARALELIZATZEKO
***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef _OPENMP
  #include <omp.h>
#else
  #define omp_get_thread_num()  0
  #define omp_get_max_threads() 1
#endif



// zenbaki konbinatorioak kalkulatzeko funtzio errekurtsiboa
long konb (int m, int n)
{
  long  r, r1, r2;


  if ((m == n) || (n == 0)) 
    r = 1;
  else
  {
    r1 = konb (m-1, n);
    r2 = konb (m-1, n-1);
    r = r1 + r2;
  }

  return (r);
}



void main (int argc, char *argv[])
{
  int   n, m, i;
  long  emaitza;


  if (argc != 3) {
    printf ("\n  eman programa eta bi zenbakiak \n\n\n");
    return;
  } 

  m = atoi (argv[1]);
  n = atoi (argv[2]);

  emaitza = konb (m, n); 

  printf ("\n   konb (%d, %d) = %ld \n\n", m, n, emaitza);
}
