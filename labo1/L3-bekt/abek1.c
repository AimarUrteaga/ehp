/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    abek1.c    

    begizten autobektorizazioa gcc-rekin
    gcc -O2 -ftree-vectorize -fopt-info-vec
**********************************************************/

#include <stdio.h>
#define N 1000


void main ( )
{
  int  i, A[N], B[N], C[N], D[N];
  int  k = 0; 


  // bektorizatzeko begiztak

  // 1. begizta
  for (i=0; i<N; i++)
  {
    A[k] = A[i] + 3 + i; 
    k = k + 1;
  }


  // 2. begizta
  for (i=2; i<N; i++)
  {
    A[i] = A[i] * A[i];
    B[i] = A[i-1] * B[i];
    C[i] = A[i-2] * C[i];
    D[i] = B[i] * C[i];
  }


  // 3. begizta
  for (i=0; i<N-2; i++) 
  {
    C[i] = B[i+2];
    B[i] = B[i] + 3; 
    //C[i] = B[i+2];
  }


  // 4. begizta
  for (i=2; i<N; i++)
  {
    D[i] = A[i-2] * 3 + 1; 
    A[i] = A[i] * D[i-1];
  } 


  // 5. begizta
  int  J;
  for (i=2; i<N-2; i++)
  {
    J=A[i+2];
    D[i] = A[i-2] + A[i+2];
    A[i] = A[i] * J;
  }

  printf("\n  %3d  %3d   %3d   %3d  \n\n", A[9], B[9], C[9], D[9]);
}

