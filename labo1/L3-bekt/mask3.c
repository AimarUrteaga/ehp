/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia

    mask3.c	OSATZEKO

    Eragiketa bektorialak maskarekin
    if (X[i] == 0)  Z[i] = Y[i] * Y[i]
**********************************************************/

#include <immintrin.h>
#include <stdio.h>
#include <unistd.h>

#define N 32

double  X[N] __attribute__ ((aligned (64))) ;
double  Y[N] __attribute__ ((aligned (64))) ;
double  Z[N] __attribute__ ((aligned (64))) ;


void op1 (double *X, double *Y, double *Z)
{
  // OSATZEKO
}


void main ( )
{
  int  i;


  for (i=0; i<N; i++) { X[i] = i%3; Y[i] = i%7; Z[i] = 0;}

  op1 (X, Y, Z);

  printf ("\n  X ");
  for (i=0; i<10; i++) printf ("%4.0f", X[i]);
  printf ("\n  Y ");
  for (i=0; i<10; i++) printf ("%4.0f", Y[i]);
  printf ("\n  Z ");
  for (i=0; i<10; i++) printf ("%4.0f", Z[i]);
  printf ("\n");
}

