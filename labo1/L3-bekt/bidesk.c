/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    abek2.c		OSATZEKO

    begizta baten exekuzio-denborak neurtzeko (time ...)
    - seriean (-O2)
    - autobektorizatuta (-O2 -ftree-vectorize -fopt-info-vec)
    - paraleloan, 4 eta 16 harirekin (-O2 -fopenmp)
    - paraleloan eta autobektorizatuta
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <immintrin.h>

#define N 8000000
double  X[N], Y[N];

double erantzuna(double  X[N], double Y[N]){
	unsigned long int i;
	__m512d vx,vy,veran;
	veran=_mm512_set1_pd(0.0);
	for (i=0; i<N; i+=8){
		vx=_mm512_load_pd(&X[i]);
		vx=_mm512_load_pd(&Y[i]);
    		veran=_mm512_fmadd_pd (vx,vy,veran);//0.1*0.2*N
	}
	return (_mm512_reduce_add_pd (veran));
}

void main ()
{
	unsigned long int  i;
	for (i=0;i<N;i++){
		X[i]=0.1;
		Y[i]=0.2;
	}
	/*
	vx=_mm512_set1_pd(0.1);
	vx=_mm512_set1_pd(0.2);
	*/
	printf("%lf",erantzuna(X,Y));
}
