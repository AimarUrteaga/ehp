/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    abek2.c		OSATZEKO

    begizta baten exekuzio-denborak neurtzeko (time ...)
    - seriean (-O2)
    - autobektorizatuta (-O2 -ftree-vectorize -fopt-info-vec)
    - paraleloan, 4 eta 16 harirekin (-O2 -fopenmp)
    - paraleloan eta autobektorizatuta
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <immintrin.h>
#include <omp.h>

#define N 1008
float  A[N][N], B[N][N], C[N][N];

void matriz(float  A[N][N],float B[N][N],float C[N][N]){
  unsigned int  i, j, k;
  __m512 va,vb,vc;
  __m512i indices=_mm512_set_epi32(0,N,2*N,3*N,4*N,5*N,6*N,7*N,8*N,9*N,10*N,11*N,12*N,13*N,14*N,15*N);
  #pragma omp parallel for private(i,j,k,va,vb,vc) shared (A,B,C,indices)
  //__m512i indices=_mm512_set_epi32(0,N,2*N,3*N,4*N,5*N,6*N,7*N,8*N,9*N,10*N,11*N,12*N,13*N,14*N,15*N);
  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++) 
    {
      //C[i][j] = 0.0;
      vc=_mm512_set1_ps(0.0);
      for (k=0; k<N; k+=16){
        va=_mm512_load_ps(&A[i][k]);
        vb=_mm512_i32gather_ps(indices,&B[k][j],1);//falta el escale factor
        vc = _mm512_fmadd_ps (va,vb,vc);//+= A[i][k] * B[k][j];
      }
      C[i][j]=_mm512_reduce_add_ps (vc);
      //if (i == 5) printf ("C5,10  %f\n", C[5][10]);
    }
  if (i == 5) printf ("C5,10  %f\n", C[5][10]);
  }
}

void main ()
{
  unsigned int  i, j, k;
#pragma omp parallel for private(i,j,k) shared (A,B)
    for (i=0; i<N; i++)
      for (j=0; j<N; j++)
        {
          A[i][j] = 1.0;
          B[i][j] = 0.5; 
        }
/*for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++) 
    {
      C[i][j] = 0.0;
      for (k=0; k<N; k++) C[i][j] += A[i][k] * B[k][j];
    }
    if (i == 5) printf ("main:C5,10  %f\n", C[5][10]);
  }
*/
  matriz(A,B,C);
}
