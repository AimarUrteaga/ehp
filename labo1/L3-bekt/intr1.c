/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    intr1.c

    Bi bektoreren batuketa intrinsics-en bidez 
    INTEL AVX 512 bit 
    Konpilatzeko: gcc -O2 -mavx512f -o intr1 intr1.c
**********************************************************/

#include <immintrin.h>		// AVX512
#include <stdio.h>
#include <time.h>

#define N 64000000

float  X[N] __attribute__ ((aligned (64))) ;
float  Y[N] __attribute__ ((aligned (64))) ;
float  Z[N] __attribute__ ((aligned (64))) ;


void main ( )
{
  int     i;
  __m512  v1, v2, v3;	// bektore-erregistroak, float

  double tex;
  struct timespec  t0, t1;


  // hasierako balioak
  for (i=0; i<N; i++) { X[i] = i%3; Y[i] = i%7; }

  clock_gettime (CLOCK_REALTIME, &t0);

  // bektore-eragiketa

  for (i=0; i<N; i+=16)			// 16 float = 512 bit
  {
    v1 = _mm512_load_ps (&X[i]);	// ps: float 
    v2 = _mm512_load_ps (&Y[i]);

    v3 = _mm512_add_ps (v1, v2);

    _mm512_store_ps (&Z[i], v3);
  }


  clock_gettime (CLOCK_REALTIME, &t1);
  tex = (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / (double)1e9;

  printf ("\n X[17] %1.2f Y[17] %1.2f Z[17] %1.2f \n", X[17], Y[17], Z[17]);
  printf ("\n Tex = %1.3f ms\n\n", tex*1000);
}

