/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    intr2.c	OSATZEKO

    bektore-eragiketak AVX512 intrinsics-ekin
**********************************************************/

#include <immintrin.h>
#include <stdio.h>

#define N 10000000
#define N2 1000000

double  A[N] __attribute__ ((aligned (64))) ;
double  B[N] __attribute__ ((aligned (64))) ;
double  C[N] __attribute__ ((aligned (64))) ;
double  D[N] __attribute__ ((aligned (64))) ;


/* bektore-kodea bektore-eragiketa hau exekutatzeko 
   D[i] = (A[i] + 10.0) * (A[i] - 10.0) / 2.0
   ADI: double
*/

void op1 (double *A, double *D)
{
	int i;
	__m512d A1,A2,VD,OP,OP1;
	OP=_mm512_set1_pd(10.0);
	OP1=_mm512_set1_pd(2.0);
	for (i=0;i<N;i+=8){
		A1=_mm512_load_pd(&A[i]);
		A2=_mm512_sub_pd(A1,OP);
		A1=_mm512_add_pd(A1,OP);
		//A2=_mm512_sub_pd(A2,OP);
		VD=_mm512_mul_pd(A2,A1);
		VD=_mm512_div_pd(VD,OP1);
		_mm512_store_pd(&D[i],VD);
	}
// OSATZEKO

}


/* Bektore-kodea begizta hau exekutatzeko
   for (i=0; i<N2; i++) {
      B[i] = C[i] / B[i];
      C[i+1] = D[i] + 1.0;
      D[i+1] = D[i+1] * D[i+1];
   }
*/
void op2 (double *B, double *C, double *D)
{

// OSATZEKO

}


void main ( )
{
  int  i;


  for (i=0; i<N; i++) { A[i] = i%3; B[i] = i%7; C[i] = 2.0; D[i] = i%13; }

  op1 (A, D);
  op2 (B, C, D);

  printf ("\n  47 osag.  > B %1.2f -- C %1.2f -- D %1.2f\n", B[47], C[47], D[47]);
}
