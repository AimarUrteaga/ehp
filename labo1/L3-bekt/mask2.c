/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    mask2.c	OSATZEKO

    Eragiketa bektorialak maskarekin
    if (X[i] < Y[i])  Z[i] = X[i] + Y[i]
               else   Z[i] = X[i];
**********************************************************/

#include <immintrin.h>
#include <stdio.h>

#define N 10000000

float  X[N] __attribute__ ((aligned (64))) ;
float  Y[N] __attribute__ ((aligned (64))) ;
float  Z[N] __attribute__ ((aligned (64))) ;


void op1 (float *X, float *Y, float *Z)
{

     // OSATZEKO

}


void main ( )
{
  int  i;


  for (i=0; i<N; i++) { X[i] = i%3; Y[i] = i%7; Z[i] = 0;}

  op1 (X, Y, Z);

  printf ("\n  X ");
  for (i=0; i<10; i++) printf ("%4.0f", X[i]);
  printf ("\n  Y ");
  for (i=0; i<10; i++) printf ("%4.0f", Y[i]);
  printf ("\n  Z ");
  for (i=0; i<10; i++) printf ("%4.0f", Z[i]);
  printf ("\n");
}

