/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    abek2.c		OSATZEKO

    begizta baten exekuzio-denborak neurtzeko (time ...)
    - seriean (-O2)
    - autobektorizatuta (-O2 -ftree-vectorize -fopt-info-vec)
    - paraleloan, 4 eta 16 harirekin (-O2 -fopenmp)
    - paraleloan eta autobektorizatuta
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define N 1000
float  A[N][N], B[N][N], C[N][N];


void main ()
{
  unsigned int  i, j, k;


  // autobektorizatzeko eta paralelizatzeko
#pragma omp parallel for private(i,j,k) shared (A,B)
  for (i=0; i<N; i++)
  for (j=0; j<N; j++)
    { A[i][j] = 1.0; B[i][j] = 0.5; }


  // autobektorizatzeko eta paralelizatzeko
#pragma omp parallel for private(i,j,k) shared (A,B,C)
  for (i=0; i<N; i++)
  {
    for (j=0; j<N; j++) 
    {
      C[i][j] = 0.0;
      for (k=0; k<N; k++) C[i][j] += A[i][k] * B[k][j];
    }
    if (i == 5) printf ("C5,10  %f\n", C[5][10]);
  }
}
