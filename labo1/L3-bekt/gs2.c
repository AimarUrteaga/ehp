/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    gs2.c	OSATZEKO

    for (i=0; i<31; i++) X[i*i] = Y[3*i] - 2.0;
**********************************************************/

#include <stdio.h>
#include <immintrin.h> 		// AVX512
#include <stdint.h>

#define N 1000
#define N2 31

double  __attribute__((aligned(64))) X[N];
double  __attribute__((aligned(64))) Y[N];


void op1 (double *X, double *Y)
{
	__m512d vconst;
	vconst=_mm512_set1_pd(2.0);
	//__m256i vindex=_mm256_setr_epi_32 (0,3,);
	int i;
	__m256i vindex=_mm256_setr_epi32 (0,3,3*2,3*3,3*4,3*5,3*6,3*7);
	for (i=0;i<N2;i+=8){
		__m512i _mm512_i32gather_pd(vindex,&Y[i*3],8);
	}
}


void main (int argc, char *argv[])
{
  int  i, j;


  for (i=0; i<N; i++) { X[i] = i; Y[i] = (2*i) % 13; }

  // hasierako balio batzuk
  printf ("\n >> hasieran");
  printf ("\n > (i=2)  X[4]   %3.0f   Y[6]  %3.0f", X[4], Y[6]);
  printf ("\n >        X[15]  %3.0f", X[15]);
  printf ("\n > (i=10) X[100] %3.0f   Y[30] %3.0f\n", X[100], Y[30]);

  op1 (X, Y);
  
  // bukaerako balioak
  printf ("\n >> bukaeran");
  printf ("\n > (i=2)  X[4]   %3.0f", X[4]);
  printf ("\n >        X[15]  %3.0f", X[15]);
  printf ("\n > (i=10) X[100] %3.0f\n", X[100]);

}

