/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    gather_scatter.c

    X[2*i] = X[2*i] * Y[2*i] + i
    batura += X[2*i]
**********************************************************/

#include <stdio.h>
#include <immintrin.h> 		// AVX512
#include <stdint.h>

#define N 64

double  __attribute__((aligned(64))) X[N];
double  __attribute__((aligned(64))) Y[N];


double op1 (double *X, double *Y)
{
  int      i;
  double   batura=0.0;
  __m512d  v1, v2, v3, v4, vakum, vi;
  __m256i  vindex;     // indize-bektorea memoria-atzipenak zehazteko


  vakum = _mm512_setzero_pd (); 	// vakum = 0, 0, 0...
  vindex = _mm256_setr_epi32 (0, 2, 4, 6, 8, 10, 12, 14);  // 2*i

  for (i=0; i<N/2; i+=8) 
  {
    v1 = _mm512_i32gather_pd (vindex, &X[2*i], 8);	// datu-bilketa, vindex arabera
    v2 = _mm512_i32gather_pd (vindex, &Y[2*i], 8);

    v3 = _mm512_mul_pd (v1, v2);
    vi = _mm512_setr_pd (i, i+1, i+2, i+3, i+4, i+5, i+6, i+7);   // i bektorea
    v4 = _mm512_add_pd (v3, vi);

    _mm512_i32scatter_pd (&X[2*i], vindex, v4, 8);	// datuak memoriara eraman, vindex arabera

    vakum = _mm512_add_pd (v4, vakum);		// bektoreen akumulazioa
  }

  batura = _mm512_reduce_add_pd (vakum);  	// reduction eragiketa: osagaien batuketa
  return (batura);
}


void main (int argc, char *argv[])
{
  int     i, j;
  double  batura;


  for (i=0; i<N; i++) { X[i] = i%10; Y[i] = 0.0; }

  batura = op1 (X, Y);
  
  for (i=0; i<8; i++) {
    for (j=0; j<8; j++) printf (" %5.1f", X[8*i+j]);
    printf ("\n");
  }
  printf ("\n  batura %1.3f \n", batura);
}

