/*  EHP - Konputagailuen Ingeniaritza - IIG, 3. maila
    SIMD laborategia -- bektorizazioa

    mask1.c	

    Eragiketa bektorialak maskara batekin
    if (i%2 == 1)  z[i] = x[i] * y[i];
            else   z[i] = 0
**********************************************************/

#include <immintrin.h>
#include <stdio.h>
#include <unistd.h>

#define N 16

float  X[N] __attribute__ ((aligned (64))) ;
float  Y[N] __attribute__ ((aligned (64))) ;
float  Z[N] __attribute__ ((aligned (64))) ;


void op1 (__mmask16 mk2, float *X, float *Y, float *Z)
{
  int     i;
  __m512  v1, v2, v3;


  for (i=0; i<N; i+=16)
  {
    v1 = _mm512_load_ps (&X[i]);
    v2 = _mm512_load_ps (&Y[i]);
    v3 = _mm512_maskz_mul_ps (mk2, v1, v2);   // biderketa maskara batekin
    _mm512_store_ps (&Z[i], v3);
  }
}


void main ( )
{
  int  i;
  __mmask16  mk2 = 43690;   // 101010101010  16 bit (float-entzat)


  for (i=0; i<N; i++) { X[i] = i; Y[i] = i; Z[i] = 0;}

  op1 (mk2, X, Y, Z);

  printf ("\n\n");
  for (i=0; i<N; i++) printf ("%5.0f", Z[i]);
  printf ("\n\n");
}

