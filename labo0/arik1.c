/*
  EHP - Konputagailuen Ingeniaritza

  arik1.c  --  serieko bertsioa
  OpenMP-ren oinarrizko kontzeptuak freskatzeko ariketa
*******************************************************/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>


#define  ERR 200000
#define  ZUT 300
#define  ESKMAX 1000
#define  BALMAX 60


double   mat[ERR][ZUT];
struct timespec  t1, t0;


static void hasieratu_mat (double mat[ERR][ZUT])
{
  int  i, j;

//#pragma omp parallel for schedule (static) private (i,j)
  for (i=0; i<ERR; i++)
  for (j=0; j<ZUT; j++)
    mat[i][j] = rand () % BALMAX - (j % 59);
}


int irakurri_eskaerak (char *feskaerak, int *zutabeak)
{
  FILE  *fitx;
  int   i, x;


  fitx = fopen (feskaerak, "r");

  i = x = 0;
  while (x != EOF) {
    x = fscanf (fitx, "%d", zutabeak + i);
    i++;
  }

  fclose (fitx);
  return (i-1);
}


void prozesatu_eskaerak (double mat[ERR][ZUT], int *zutabeak, int kop, double *emaitzak)
{
  int     i, j, zutabe;
  double  y, batura;

//#pragma omp parallel for schedule (static) private (i,j,zutabe,y,batura)
//#pragma omp parallel for schedule (dynamic) private (i,j,zutabe,y,batura)
#pragma omp parallel for schedule (guided) private (i,j,zutabe,y,batura)
  for (j=0; j<kop; j++)
  {
    zutabe = zutabeak[j];
    batura = 0.0;
//#pragma omp parallel for schedule (static) private (i,j,zutabe,y) reduction (+:batura)
    for (i=0; i<ERR; i++)
    {
      y = mat[i][zutabe];
      if (y > 0) batura += exp (y / 100);
    }
    emaitzak[j] = batura;
  }
}


int main (int argc, char *argv[])
{
  int     esk_kop, zutabeak[ESKMAX];
  double  tex, *emaitzak;


  hasieratu_mat (mat);

  esk_kop = irakurri_eskaerak (argv[1], zutabeak);
  emaitzak = malloc (esk_kop * sizeof (double));

  clock_gettime (CLOCK_REALTIME, &t0);

  prozesatu_eskaerak (mat, zutabeak, esk_kop, emaitzak);

  clock_gettime (CLOCK_REALTIME, &t1);

  tex = (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) /(double) 1e9;
  printf ("\n >> Eskaerak: %d -- Azken batura: %1.3f -- Tex: %1.1f ms\n\n", esk_kop, emaitzak[esk_kop - 1], tex*1000);

  return (0);
} 

